import Vue from 'vue'
import Router from 'vue-router'
import Landing from '../src/components/index'
import Loading from '../src/components/loading'
import Redeem from '../src/components/redeem'

Vue.use(Router)

const router = new Router ({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'Landing',
            component: Landing
        },
        {
            path: '/loading',
            name: 'Loading',
            component: Loading
        },
        {
            path: '/redeem',
            name: 'Redeem',
            component: Redeem
        },
        {
            path: '/auth',
            beforeEnter() {
                window.location.href = 'https://api.instagram.com/oauth/authorize' + 
                  '?client_id=' + 198533864862091 +
                  '&redirect_uri=' + 'https://localhost:8080/loading/' +
                  '&scope=user_profile,user_media' +
                  '&response_type=code'
            }
        }
    ]
})

export default router