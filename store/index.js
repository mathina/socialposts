import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        postImageUrl: '',
        accountId: '',
        redeemSuccess: false,
        facebookHashtag: 'FineLine',
        instagramHashtag: 'FineLine',
        platform: null,
        facebookLoggedIn: false,
        facebookMode: 'limit',
        // facebookMode: 'today',
        facebookLimit: 100,
        instagramMode: 'limit',
        // instagramMode: 'today',
        instagramLimit: 100,
        eventId: '77759997-6aa1-4bb1-c4aa-222207f9e343',
        deviceId: 'b96d00c1-8f6c-4e6d-afdf-22bf553c2a46',
        notRefresh: false
    },
    getters: {
        postImageUrl: state => state.postImageUrl,
        accountId: state => state.accountId,
        redeemSuccess: state => state.redeemSuccess,
        facebookHashtag: state => state.facebookHashtag,
        instagramHashtag: state => state.instagramHashtag,
        platform: state => state.platform,
        facebookLoggedIn: state => state.facebookLoggedIn,
        facebookMode: state => state.facebookMode,
        facebookLimit: state => state.facebookLimit,
        instagramMode: state => state.instagramMode,
        instagramLimit: state => state.instagramLimit,
        eventId: state => state.eventId,
        deviceId: state => state.deviceId,
        notRefresh: state => state.notRefresh
    },
    mutations: {
        updatePostImageUrl: (state, value) => {
            state.postImageUrl = value
        },
        updateAccountId: (state, value) => {
            state.accountId = value
        },
        updateRedeemSuccess: (state, value) => {
            state.redeemSuccess = value
        },
        updatePlatform: (state, value) => {
            state.platform = value
        },
        updateNotRefresh: (state, value) => {
            state.notRefresh = value
        }
    }
})

export default store